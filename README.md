<img src="./docs/20210820o1133.blank-wof-1-3162961.v0.x0128y0128.png" align="right" width="128" height="128" alt="Wheel-Of-Fortune icon">

# Wheel Of Fortune &nbsp; <sup><sub><sup>v0.1.3</sup></sub></sup>

Aloha!

## Synopsis

- Slogan : Single-file JavaScript control to pick a random name or number from a HTML page.

- Summary : This is a small JavaScript control to pick a name or a
 random integer between one and any desired. It comes with a sound effect
 like that of a real wheel of fortune.

- Description :
 The control may look like this :
 <br>
 ![The control on the page](./docs/imgs/20211123o1132.wheel-of-fortune-with-numbers.png)
 &nbsp; <span style="position:relative; bottom:70px;">or</span> &nbsp;
 ![The control on the page](./docs/imgs/20211123o1131.wheel-of-fortune-screenshot.png)
 <br>
 It operates like follows.
 (1) Left image, number mode — In the 'Max' field, input the highest number you want get.
 (2) Right image, names mode — If you have activated the [names list](./wheel-of-fortune-config.js),
 instead the 'Max' field, the number of names is shown, from which one is randomly hit.
 (3) Press the 'Spin' button. (4) For three seconds, the knacking
 of a slowing down wheel-of-fortune is played, while the display changes.
 (5) The 'wheel' stops, showing the result.
 (6) With the 'X' button you can clear the result field.

- Project status : It works.

- License : [BSD 3-Clause License](./license.txt) (see digest on [tldrlegal.com/…](https://tldrlegal.com/license/bsd-3-clause-license-(revised)))

## Features

- The output number is an integer between 1 and the number set in the 'Max' field.

- If a names list is provided, strings will be shown instead of numbers.

- On action, it emits a three second knacking sound, starting fast,
 then slowing down until it stops and shows the result.

- Compatibility :
 No Internet Explorer, otherwise all reasonably new browsers.
 For Howler.js compatibility in particular see e.g.
 [ninodezign.com/&#8203;howler-&#8203;js-&#8203;modern-&#8203;web-&#8203;audio-&#8203;javascript-&#8203;library/](http://ninodezign.com/howler-js-modern-web-audio-javascript-library/) <sup><sub>*[ref 20210820°1622]*</sub></sup>

- Please note, it does not (yet?) resemble a real wheel-of-fortune algorithm.
 It is just a random-number-picker with a wheel-of-fortune sound effect.

## Usage

(1) Point to the script in the head of your HTML page :

```
   <script src="./wheel-of-fortune.js"></script>
```

(2) In the HTML body, place a `div` with **`id="Wheel-Of-Fortune"`**. The
 control will be inserted into this `div`. The `div` may look simple like this :

```
   <div id="Wheel-Of-Fortune"></div>
```

(3) Optionally, in the HTML file, you can overwrite the control's built-in formatting,
 e.g. like is done in the [`docs/demo.html`](./docs/demo.html) page :

```
   <div style="background-color:Yellow;" id="Wheel-Of-Fortune">
   </div>
```

(4) If you additionally provide the optional configuration script
 with an array of names, names instead of numbers will be shown :

```
   <script src="./wheel-of-fortune.js"></script>
   <script src="./../somewhere/wheel-of-fortune-config.js"></script>
```

For the exact format of that list see the [example script](./wheel-of-fortune-config.js).

## Issues

- On a newly loaded page, for the first round, the sound comes with about two
 seconds delay, so the **first knacks are missing**.
 &nbsp; <sup><sub><sup>*[issue 20210820°1421 'Initial sound fail']*</sup></sub></sup>

- The Edge and Chrome debuggers may show **warnings** or web hints. Not really bad,
 just annoying. Not sure they all can be obeyed, but we could have a look at it.
 &nbsp; <sup><sub><sup>*[issue 20210822°1611 'Browser hints']*</sup></sub></sup>

- Works not like a real wheel of fortune. Such wheel works with a fixed
 sequence of items, then stopping at a random position. But this here now
 works with a random sequence of items stoping predetermined. Todo:
 Implement a **more wheel-like algorithm**, possibly with graphic animation.
 &nbsp; <sup><sub><sup>*[todo 20210822°1541 'More realistic algo']*</sup></sub></sup>

- Allow **multiple WOFs** on a page, especially one for numbers, one for names.
 To allow this is not soo easy, quite some restructuring were necessary.
 (1) Do no more use an element ID for placing the WOF on the page, but e.g.
  a CSS class. (2) Use different audio objects for the different WOFs,
 ideally, the different WOFs can also had different sounds.
 &nbsp; <sup><sub><sup>*[todo 20210823°1151 'Allow multiple WOFs']*</sup></sub></sup>

- Find a more specific project name, since 'wheel-of-fortune' exist myriads.

## References

This are some references on related topics :

<img src="./docs/imgs/20220128o1733.wheelofnames.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for wheelofnames.com">
 &nbsp;
 A sophisticated online service is
 <a href="https://wheelofnames.com/" style="font-weight:bold;" data-webshot="20220128°1732" id="">wheelofnames.com</a>
 . Their FAQ page
 <a href="https://wheelofnames.com/faq/randomness" data-webshot="20220128°1734" id="">randomness</a>
 tells details about the wheel's randomness.

 &nbsp;

<img src="./docs/imgs/20200913o0244.hackernoon.v1.x0048x0048.png" align="left" width="48" height="48" alt="Link icon for HackerNoon">
 &nbsp;
 HackerNoon article
 <a href="https://hackernoon.com/how-does-javascripts-math-random-generate-random-numbers-ef0de6a20131" data-webshot="20220128°1735" id="">How does JavaScript’s Math.random() generate random numbers?</a>

&nbsp;

## Credits

Many thanks goes to the following people :

<img src="./docs/imgs/20210820o1523.howlerjs.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for HowlerJs">
 The authors of [howlerjs.com](https://howlerjs.com/)
 for their permissively licensed cool audio library

&nbsp;

<img src="./docs/imgs/20210820o1123.clipartkey.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for ClipArtKey">
 Edith Shelly for the logo on
 [www.clipartkey.com/&#8203;view/&#8203;ihxbRxo_&#8203;transparent-&#8203;twister-&#8203;game-&#8203;clipart-&#8203;wheel-&#8203;of-&#8203;fortune-&#8203;blank/](https://www.clipartkey.com/view/ihxbRxo_transparent-twister-game-clipart-wheel-of-fortune-blank/)
 <sup><sub><sup>*[ref 20210820°1132]*</sup></sub></sup>

&nbsp;

## Finally

Find my other projects listed on
 [github.com/normai/](https://github.com/normai/)

Bye
<br>Norbert
<br>2023-Sept-23 &nbsp; <del>2022-June-06</del> &nbsp; <del>2022-Jan-29</del> &nbsp; <del>23-Nov-2021</del>

<sup style="position:relative; top:1.1em; color:LightGray;"><sub><sup>*[File 20210820°1207]* ܀Ω</sup></sub></sup>
