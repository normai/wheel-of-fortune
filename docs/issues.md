﻿### Issues

This file stores issues, todos, questions and the like.

---

Feature request 20230113°1140.
 When working with a participant list, there shall be a comfortable way
 to temporarily exclude specific participants. E.g. the list shall be
 shown and by setting a checkmark that participant is skipped when
 turning the wheel.

Todo 20220718°1821 — Make the control's width configurable. Compare changes `20220718°1821/1822/1823`

---

#### MS-Edge warnings &nbsp; <sup><sub><sup>*[issue 20210823°1131 'Edge warnings']*</sup></sub></sup>

Matter : MS-Edge shows several warnings, which may or may not be easy to fix.

Priority : Low.

Status : Open

---

#### Where exactly shall the audio object be created? &nbsp; <sup><sub><sup>*[issue 20210823°1111 'Where place new Howl()']*</sup></sub></sup>

So far, I tried two places for creating the audio object
`oSound = new Howl`, and a third place might be tried.

In general, I think it is no problem to have multiple audio objects for one
 WOF, the JavaScript garbage collector should care about them.

##### (1) Inside function Tick()

Placing inside function 20210626°0751 Tick() was the case until v.0.0.3.
 This place seems the dirtiest one, because on each timer tick a new audio
 object is created. I suspected this the reason for issue 20210820°1421
 'Initial knacks missing', but it was not.

##### (2) Inside ExecuteOnPageLoad(), long before the play() call.

Creating the Howl object immediately on page load is how it is done right
 now (in v0.0.7). This looks the most natural place to provide the object.
 Just, this did not solve issue 20210820°1421 'Initial sound fail'.

In Google Chrome we get two warnings *"The AudioContext was not allowed
 to start. It must be resumed (or created) after a user gesture on the page."*
 with a reference to
 [developer.chrome.com/blog/autoplay/](https://developer.chrome.com/blog/autoplay/)
<sup><sub><sup>*[ref 20210823°1113]*</sup></sub></sup>.

StackOverflow thread
 [Howler.js - The AudioContext was not allowed to start. It must be resumed (or created) after a user gesture on the page](https://stackoverflow.com/questions/67686203/howler-js-the-audiocontext-was-not-allowed-to-start-it-must-be-resumed-or-cr)
<sup><sub><sup>*[ref 20210823°1114]*</sup></sub></sup>
 discusses the issue.

##### (3) After button press inside SpinTheWheel()

The place at the beginning function 20210626°0741 SpinWheel() seems 
 also suited. The object were created 'on demand', only after button press.

Not yet tried.

---

#### Finished Issues

- <del>Allow an optional JavaScript file with an **array of student names**,
 so the wheel does not show abstract numbers but immediate names. This should
 be done by a JavaScript file, because another file type had to be loaded
 via AJAX, and this will no more work viewing the page via file protocol.
 &nbsp; <sup><sub><sup>*[todo 20210823°1141 'Provide names list']*</sup></sub></sup></del>
 Status : Done with v0.0.7 'Names provision'.

#### <del>Typo in `wheel-of-fortune-config.js` &nbsp; <sup><sub><sup>*[issue 20210916°1751 'Typo']*</sup></sub></sup></del>

<del>The array wrongly spells 'WAF\_ITEMS', it shall spell 'WOF\_ITEM\_NAMES'.</del>
 Status : Done with v0.1.0 '..'.

---

<sup><sub><sup>*[File 20210820°1209]* ܀Ω</sup></sub></sup>
