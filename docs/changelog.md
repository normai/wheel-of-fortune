﻿### Changelog


version 20230923°1151 — v0.1.3 'Set faster'
 - Make knacking a bit faster

version 20220718°1831 — v0.1.2 'Make Wider'
 - Make the control a bit wider

version 20220606°1551 — v0.1.1 'Wider'
 - Make the control a bit wider

chg 20220128°1841 — Intermediate
 - Little fixes
 - Edit docs

version 20211123°1151 — v0.1.0 'Refine'
 - Rename `WAF_ITEMS` to `WOF_ITEM_NAMES`
 - Rename `wheel-of-fortune-names.js` to `wheel-of-fortune-config.js`
 - Style control from JavaScript no more from HTML

tag 20210827°1515 — v0.0.8 'Little cleanup'

tag 20210824°0911 — v0.0.7 'Names provision'
 - Introduce names provision via optional script

tag 20210823°1212 — v0.0.6 'Add docs folder'
 - Add documentation folder, shift files there
 - More tries to solve the sound-delay issue (not succeeded)

tag 20210822°1621 — v0.0.5
 - Shift tweaking constants to config object on file top
 - Try solve issue 20210820°1421 'Start quirks' (not succeeded)

log 20210820°1818 — v0.0.4 — Fix issue 20210820°1421 'Sound starts delayed'

log 20210820°1717 — v0.0.3 — Initial upload

log 20210626°0821 — v0.0.2 — Initial idea

<sup><sub><sup>*[File 20210820°1208]* ܀Ω</sup></sub></sup>
