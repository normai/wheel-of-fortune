﻿/*!
 * file : 20210823°1351 wheel-of-fortune-config.js
 * summary : Optional supplement beside wheel-of-fortune.js. If the array
 *           WOF_ITEM_NAMES exists, the wheel shows strings instead numbers.
 * note : Why might you want to locate this file outside the folder where
 *        the page resides? E.g. if the WOF is on a public page, but the
 *        names list shall not published.
 * note : Why is this file written as JavaScript file, instead as a JSON file?
 *        Because AJAX will not work if the page is viewed via file protocol,
 *        due to recent browser policies. But JavaScript does work anyway.
 */

// Prologue
"use strict";
var Trekta = Trekta || {};
Trekta.Wof = Trekta.Wof || {};

// Configuration values

// This must be zero (smaller than one) that below names list is applied
Trekta.Wof.WOF_ITEMS_NUM = 0; // 21;

// Names list, is only used, if above WOF_ITEMS_NUM is zero
Trekta.Wof.WOF_ITEM_NAMES =
[
     'Alice'
   , 'Äronwen'
   , 'Bob'
   , 'Cleopatra'
   , 'Doris'
   , 'Emil'
   , 'Fritz'
   , 'Gunnar'
   , 'Hakizimana'
   , 'Isabella'
   , 'Jenny'
   , 'Kaleialoha'
   , 'Lavender'
   , 'Magnolia'
   , 'Nikolaus'
   , 'Otto'
   , 'Øyvind'
   , 'Phillip'
   , 'Quentin'
   , 'Ragnvaldr'
   , 'Siegfried'
   , 'Thusnelda'
   , 'Uhura'
   , 'Üli'
   , 'Valentin'
   , 'Wendy'
   , 'Xenophon'
   , 'Yankee'
   , 'Zuleikha'
];
